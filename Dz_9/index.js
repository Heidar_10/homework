

//Описать своими словами в несколько строчек, зачем в программировании нужны циклы.
//Циклы используются в коде,  чтобы повторить кусок кода нужное количество раз и чтобы сэкономить место памяти.


let userNumber = +prompt('Enter the number');
while(userNumber === null || isNaN(Number(userNumber))){
    userNumber = parseInt(prompt('Enter the number'));
}
const number = 5;
if (userNumber < number) {
	console.log('Sorry, no numbers');
} else {
	for (let i = 0; i <= userNumber; i++){
		if (i % number === 0) {
			console.log(i);
		}
	}
}
