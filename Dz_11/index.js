//Метод это свойство, содержащее определение функции

function createNewUser() {
  this.firstName = prompt("Enter you name: ", "");
  while (this.firstName === "") {
    this.firstName = prompt("Enter you name AGAIN: ", "");
  }

  this.lastName = prompt("Enter you surname", "");
  while (this.lastName === "") {
    this.lastName = prompt("Enter you surname AGAIN: ", "");
  }

  this.getLogin = function () {
    let newLogin =
      this.firstName.charAt(0).toLowerCase() + this.lastName.toLowerCase();
    return newLogin;
  };
}

let newUser = new createNewUser();
alert(`Your login is: ${newUser.getLogin()}`);
